package ru.innopolis.db;

import java.sql.*;

/**
 * by Andrey Kostrov on 22.11.2016.
 */
public class SqlDB {

    private static final String PREPARED_INSERT_QUERY = "INSERT INTO USERS VALUES(?, ?, ?, ?, ?);";
    private static final String PREPARED_SELECT_QUERY = "SELECT * FROM USERS";
    private static final String PREPARED_CHECK_QUERY =  "SELECT * FROM USERS WHERE LOGIN=? ";
    private static final String PREPARED_PASS_CHECK_QUERY =  "SELECT PASS FROM USERS WHERE LOGIN=? ";
    private static final String PREPARED_ADMIN_CHECK_QUERY =  "SELECT ISADMIN FROM USERS WHERE NAME=?";
    private static ConnectionPool connectionPool;
    //public static Connection conn;

    static {

        String urlDB = "jdbc:h2:C:\\Program Files (x86)\\apache-tomcat-9.0.0.M13\\bin\\usersDB";
        String driverClass = "org.h2.Driver";

            connectionPool = new ConnectionPool(urlDB, driverClass, 10);
            //conn = connectionPool.retrieve();

    }

    public static void inintialize(){
        try {
            Connection conn = connectionPool.retrieve();
            Statement statement = conn.createStatement();

            statement.execute("CREATE TABLE USERS(ID INT PRIMARY KEY, LOGIN VARCHAR(255), PASS VARCHAR(255)," +
                                                    " EMAIL VARCHAR(255), ISADMIN BOOL)");
            PreparedStatement addStatement = conn.prepareStatement(PREPARED_INSERT_QUERY);
            addStatement.setInt(1, SqlDB.getMaxID() + 1 );
            addStatement.setString(2, "admin");
            addStatement.setString(3, "admin");
            addStatement.setString(4, "admin@admin.com");
            addStatement.setString(5, "TRUE");

            addStatement.executeUpdate();
            connectionPool.putback(conn);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean addUser(String name, String pass, String email){
        try {
            Connection conn = connectionPool.retrieve();

            PreparedStatement checkStatement = conn.prepareStatement(PREPARED_CHECK_QUERY);
            checkStatement.setString( 1 , name);

            if (!checkStatement.executeQuery().first()) {

                PreparedStatement addStatement = conn.prepareStatement(PREPARED_INSERT_QUERY);
                addStatement.setInt(1, SqlDB.getMaxID() + 1 );
                addStatement.setString(2, name);
                addStatement.setString(3, pass);
                addStatement.setString(4, email);
                addStatement.setString(5, "FALSE");

                addStatement.executeUpdate();
                connectionPool.putback(conn);
                return true;
            }
            connectionPool.putback(conn);
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean checkUser(String login, String password){
        Connection conn = null;
        try {
            boolean result = false;

            conn = connectionPool.retrieve();
            PreparedStatement checkExistStatement = conn.prepareStatement(PREPARED_CHECK_QUERY);
            checkExistStatement.setString( 1 , login);
            if (checkExistStatement.executeQuery().first()) {
                PreparedStatement checkStatement = conn.prepareStatement(PREPARED_PASS_CHECK_QUERY);
                checkStatement.setString(1, login);
                ResultSet table = checkStatement.executeQuery();
                table.next();
                if (password.equals(table.getString(1)))
                    result = true;
            }
            connectionPool.putback(conn);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static ResultSet getWholeTable(){
        try {
            Connection conn = connectionPool.retrieve();
            Statement statement = conn.createStatement();
            ResultSet table = statement.executeQuery(PREPARED_SELECT_QUERY);
            connectionPool.putback(conn);
            return table;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isAdmin(String name){

        try {
            Connection conn = connectionPool.retrieve();
            PreparedStatement statement = conn.prepareStatement(PREPARED_ADMIN_CHECK_QUERY);
            statement.setString(1, name);
            ResultSet table = statement.executeQuery(PREPARED_SELECT_QUERY);
            boolean result = table.getBoolean(1);
            connectionPool.putback(conn);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static int getMaxID(){
        try {
            Connection conn = connectionPool.retrieve();
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT MAX(ID) FROM USERS");
            resultSet.next();
            int maxID = resultSet.getInt(1);
            connectionPool.putback(conn);
            return maxID;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }


}
