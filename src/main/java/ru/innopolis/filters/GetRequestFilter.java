package ru.innopolis.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;

/**
 * by Andrey Kostrov on 23.11.2016.
 */
public class GetRequestFilter implements Filter {

    private FilterConfig filterConfig;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException
    {
            HttpServletRequest req = (HttpServletRequest)request;
            if (req.getMethod().equals("GET")) {
                filterConfig.getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
            }
            filterChain.doFilter(request, response);
    }

    @Override
    public void destroy()
    {
        filterConfig = null;
    }
}
