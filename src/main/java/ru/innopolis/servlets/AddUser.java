package ru.innopolis.servlets;

/**
 * by Andrey Kostrov on 19.11.2016.
 */

import ru.innopolis.db.SqlDB;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddUser extends HttpServlet {

    public String getServletInfo(){
        return "Add user servlet";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/registration.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        ServletContext ctx = getServletContext();
        /* validating entered data (simple NOT_NULL validation) */
        if (request.getParameter("userName").equals("")) {
            request.setAttribute("error", "LOGIN field is empty");
            getServletContext().getRequestDispatcher("/registration.jsp").forward(request, response);
        }
        else if(request.getParameter("password").equals("")) {
            request.setAttribute("error", "PASSWORD field is empty");
            getServletContext().getRequestDispatcher("/registration.jsp").forward(request, response);
        }
        else if (request.getParameter("email").equals("")){
            request.setAttribute("error", "EMAIL field is empty");
            getServletContext().getRequestDispatcher("/registration.jsp").forward(request, response);
        }
        else {

            /*if all input data valid - creating new user instance*/
            String userName = request.getParameter("userName");
            String password = request.getParameter("password");
            String email = request.getParameter("email");

            /*trying to add new user to system. Return false if userName already used by another user*/
            boolean res = SqlDB.addUser(userName, password, email);
            if (!res) {
                request.setAttribute("error", "user  " + userName + "  already exists in system");
                getServletContext().getRequestDispatcher("/registration.jsp").forward( request, response);
            } else {
                getServletContext().getRequestDispatcher("/Login").forward( request, response);
            }
        }
    }
}
