package ru.innopolis.servlets;

import ru.innopolis.db.SqlDB;
import ru.innopolis.deprecated.UserList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * by Andrey Kostrov on 19.11.2016.
 */
public class CheckUser extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(request.getSession().getAttribute("currentUser") != null)
            if((Boolean) request.getSession().getAttribute("enteredCabinet")) {
                request.setAttribute("table", SqlDB.getWholeTable());
                getServletContext().getRequestDispatcher("/mainPage.jsp").forward(request, response);
            }

        if (request.getParameter("userName").isEmpty()) {
            request.setAttribute("error", "LOGIN field is empty");
            getServletContext().getRequestDispatcher("/Login").forward(request, response);
        }
        else if(request.getParameter("password").equals("")) {
            request.setAttribute("error", "PASSWORD field is empty");
            getServletContext().getRequestDispatcher("/Login").forward(request, response);
        }
        else {
//            if (UserList.findUser(request.getParameter("userName")) != null) {
//                if (UserList.findUser(request.getParameter("userName")).getPassword().equals(request.getParameter("password"))) {
                if (SqlDB.checkUser(request.getParameter("userName"), request.getParameter("password"))){
                    /*---Adding "entered" attribute to session---*/
                    request.getSession().setAttribute("enteredCabinet", Boolean.TRUE);
                    request.getSession().setAttribute("currentUser", request.getParameter("userName"));
                    request.setAttribute("table", SqlDB.getWholeTable());
                    getServletContext().getRequestDispatcher("/mainPage.jsp").forward(request, response);

                }
                else{
                    request.setAttribute("error", "Wrong Login or Password");
                    getServletContext().getRequestDispatcher("/Login").forward(request, response);
                }
        }
    }
}
