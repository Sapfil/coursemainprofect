package ru.innopolis.servlets;

import ru.innopolis.db.SqlDB;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;

/**
 * by Andrey Kostrov on 19.11.2016.
 */
public class Login extends HttpServlet {

//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        request.setAttribute("table", SqlDB.getWholeTable());
//        getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
//    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(true);

        //SqlDB.inintialize();

        if(session.getAttribute("enteredCabinet") == null|| request.getParameter("exit") != null) {
            session.setAttribute("enteredCabinet", Boolean.FALSE);
            session.setAttribute("currentUser", null);
        }

        request.setAttribute("table", SqlDB.getWholeTable());
        getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);

    }
}